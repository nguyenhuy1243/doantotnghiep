package com.example.nguye.user.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.nguye.user.Fragment.fragment_cencle;
import com.example.nguye.user.Fragment.fragment_done;
import com.example.nguye.user.Fragment.fragment_pendding;

import java.util.ArrayList;
import java.util.List;

public class ViewpagerAdapterOder  extends FragmentPagerAdapter {
    List<Fragment> listFragment = new ArrayList<Fragment>();
    List<String> tittleFragment = new ArrayList<String>();
    public ViewpagerAdapterOder(FragmentManager fm) {
        super(fm);
        listFragment.add(new fragment_pendding());
        listFragment.add(new fragment_done());
        listFragment.add(new fragment_cencle());

        tittleFragment.add("Đang chờ");
        tittleFragment.add("Hoàn thành");
        tittleFragment.add("Hủy bỏ");
    }

    @Override
    public Fragment getItem(int position) {
        return listFragment.get(position);
    }

    @Override
    public int getCount() {
        return listFragment.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tittleFragment.get(position);
    }
}

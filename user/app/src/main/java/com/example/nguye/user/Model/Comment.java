package com.example.nguye.user.Model;

import java.io.Serializable;

public class Comment implements Serializable {
    public String idComment;
    public String idUser ;
    public String idDriver;
    public String comment;
    public Double evaluate;

    public Comment() {
    }

    public Comment(String idComment, String idUser, String idDriver, String comment, Double evaluate) {
        this.idComment = idComment;
        this.idUser = idUser;
        this.idDriver = idDriver;
        this.comment = comment;
        this.evaluate = evaluate;
    }

    public String getIdComment() {
        return idComment;
    }

    public void setIdComment(String idComment) {
        this.idComment = idComment;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdDriver() {
        return idDriver;
    }

    public void setIdDriver(String idDriver) {
        this.idDriver = idDriver;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getEvaluate() {
        return evaluate;
    }

    public void setEvaluate(Double evaluate) {
        this.evaluate = evaluate;
    }
}

package com.example.nguye.user.Activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.nguye.user.Activity.Driver.ManagerDriverActivity;
import com.example.nguye.user.Activity.Oder.DetailsOderActivity;
import com.example.nguye.user.Activity.Oder.ManagerAllOder;
import com.example.nguye.user.Activity.user.DetailUserActivity;
import com.example.nguye.user.Activity.user.EditUserActivity;
import com.example.nguye.user.Contants.Contant;
import com.example.nguye.user.Fragment.fragment_dangnhap;
import com.example.nguye.user.Model.Oder;
import com.example.nguye.user.Model.User;
import com.example.nguye.user.ModuleMap.DirectionFinder;
import com.example.nguye.user.ModuleMap.DirectionFinderListener;
import com.example.nguye.user.ModuleMap.Route;
import com.example.nguye.user.R;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class    MainActivity extends AppCompatActivity {
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;
    private GoogleMap mMap;
    private final static int MY_PERMISSION_FINE_LOCATION = 101;
    private Button btnbatdau;
    private ProgressDialog progressDialog;
    private  Oder mOder;
    private User mUser;
    TextView userNameNV,emailNV;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Users");
    private String origin;
    private String destination;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        anhXa();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(Gravity.START);
            }
        });
        //lay user tu database
        myRef.child(Contant.idAuth).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                 mUser =  dataSnapshot.getValue(User.class);
                 userNameNV.setText(mUser.getUsername());
                 emailNV.setText(mUser.getEmail());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.chuyendi:{
                        Toast.makeText(MainActivity.this, "1", Toast.LENGTH_SHORT).show();
                        break;
                    }
                    case R.id.map_it:{
                        startActivity(new Intent(MainActivity.this,MapsActivity.class));
                        break;
                    }
                    case R.id.lichsu:{
                        startActivity(new Intent(MainActivity.this, ManagerAllOder.class));
                        break;
                    }
                    case R.id.seting:{
                        Intent intent = new Intent(MainActivity.this, DetailUserActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", Contant.idAuth);
                        intent.putExtra("data", bundle);
                        startActivity(intent);
                        break;
                    }
                    case R.id.dangxuat:{
                        Intent intent = new Intent(MainActivity.this,LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    }
                }
                return false;
            }
        });
        Log.e("idauth", Contant.idAuth);
        btnbatdau.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,MapsActivity.class));
            }
        });
    }

    private void anhXa() {
        drawerLayout = findViewById(R.id.dl);
        toolbar= findViewById(R.id.toolbar);
        navigationView= findViewById(R.id.nav_view);
        btnbatdau = findViewById(R.id.btnbatdau);
        View header = navigationView.getHeaderView(0);
        userNameNV = header.findViewById(R.id.usernamenv);
        emailNV = header.findViewById(R.id.emailnv);

    }

}

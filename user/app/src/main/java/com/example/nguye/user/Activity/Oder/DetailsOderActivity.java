package com.example.nguye.user.Activity.Oder;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.nguye.user.Activity.MainActivity;
import com.example.nguye.user.Activity.MapsActivity2;
import com.example.nguye.user.Contants.Contant;
import com.example.nguye.user.Model.Comment;
import com.example.nguye.user.Model.Oder;
import com.example.nguye.user.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;

public class DetailsOderActivity extends AppCompatActivity {
    Toolbar toolbardetailOrder;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Oders");
    EditText edArrivalOder,edvipOder;
    TextView txtoriginAddress,txtdestinationAddress,txtEstimatetime,txtphone,txtPriceOder,txtStatusOder;
    Button btnComment,btnCencle,btnChuyendi;
    Oder oder;
    Comment comment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_oder);
        anhXa();
        setSupportActionBar(toolbardetailOrder);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Chi tiết");
        toolbardetailOrder.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbardetailOrder.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailsOderActivity.this, MainActivity.class));
                finish();
            }
        });
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("data");
        if (bundle != null) {
            final String idOder = bundle.getString("idOder");
            Log.e("id",idOder);
            myRef.child(idOder).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    oder =  dataSnapshot.getValue(Oder.class);
                    edArrivalOder.setText(oder.getArrivalTime());
                    edvipOder.setText(oder.getVip());
                    txtdestinationAddress.setText(oder.getDestinationAddress());
                    txtoriginAddress.setText(oder.getOriginAddress());
                    //txtEstimatetime.setText(oder.getExtimateTime());
                    txtphone.setText(oder.getPhone());
                    txtPriceOder.setText(oder.getPrice()+"");
                    if(oder.getStatusOder().equals(Contant.PENDING)){
                        txtStatusOder.setText(Contant.PENDING_txt);
                        btnComment.setVisibility(View.INVISIBLE);
                    }else if(oder.getStatusOder().equals(Contant.CONFIRN)){
                        btnComment.setVisibility(View.INVISIBLE);
                        txtStatusOder.setText(Contant.CONFIRN_txt);
                    }else if(oder.getStatusOder().equals(Contant.DONE)){
                        txtStatusOder.setText(Contant.DONE_txt);
                        btnComment.setVisibility(View.VISIBLE);
                        btnCencle.setVisibility(View.INVISIBLE);
                        btnChuyendi.setVisibility(View.INVISIBLE);
                    }else if(oder.getStatusOder().equals(Contant.CENCLE)){
                        txtStatusOder.setText(Contant.CENCLE_txt);
                        btnComment.setVisibility(View.VISIBLE);
                        btnChuyendi.setVisibility(View.INVISIBLE);
                        btnCencle.setVisibility(View.INVISIBLE);
                    }
                    btnCencle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            oder.setStatusOder(Contant.CENCLE);
                            myRef.child(idOder).setValue(oder);
                        }
                    });
                    btnComment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            comment = new Comment();
                            comment.setIdUser(oder.getIdUser());
                            comment.setIdDriver(oder.getIdDriver());
                            comment.setIdComment(oder.getIdOder());
                            Bundle bundle = new Bundle();
                            Intent intentComment = new Intent(DetailsOderActivity.this,Commenttaixe.class);
                            bundle.putSerializable("comment", (Serializable) comment);
                            intentComment.putExtra("data",bundle);
                            startActivity(intentComment);
                        }
                    });
                    btnChuyendi.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            oder.setStatusOder(Contant.CONFIRN);
                            myRef.child(idOder).setValue(oder);
                            Intent intentmap = new Intent(DetailsOderActivity.this, MapsActivity2.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("idOder", idOder);
                            intentmap.putExtra("data",bundle);
                            startActivity(intentmap);
                        }
                    });

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

    }

    private void anhXa() {
        toolbardetailOrder = findViewById(R.id.toolbaroder);
        edArrivalOder = findViewById(R.id.edarrivaloder);
        edvipOder = findViewById(R.id.edvipoder);
        txtdestinationAddress = findViewById(R.id.destinationaddress);
        txtoriginAddress = findViewById(R.id.originaddress);
        txtEstimatetime = findViewById(R.id.txtestimatetime);
        txtphone = findViewById(R.id.txtphone);
        txtPriceOder = findViewById(R.id.txtpriceoder);
        txtStatusOder = findViewById(R.id.txtstatusoder);
        btnCencle = findViewById(R.id.btncelcle);
        btnComment = findViewById(R.id.btncomment);
        btnChuyendi = findViewById(R.id.btnchuyendi);
    }
}

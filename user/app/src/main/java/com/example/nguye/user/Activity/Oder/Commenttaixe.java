package com.example.nguye.user.Activity.Oder;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nguye.user.Activity.Driver.ManagerDriverActivity;
import com.example.nguye.user.Activity.MainActivity;
import com.example.nguye.user.Contants.Contant;
import com.example.nguye.user.Model.Comment;
import com.example.nguye.user.Model.Oder;
import com.example.nguye.user.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hsalf.smilerating.SmileRating;

import java.io.Serializable;

public class Commenttaixe extends AppCompatActivity {
    Toolbar toolbarComment;
    EditText edComment;
    Intent intent;
    private Comment comment;
    Button btnConfirn;
    SmileRating smileRating;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Comments");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        anhXa();
        setSupportActionBar(toolbarComment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Đơn hàng");
        toolbarComment.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbarComment.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Commenttaixe.this, MainActivity.class));
                finish();
            }
        });
        intent = getIntent();
        final Bundle bundle = intent.getBundleExtra("data");
        if(bundle!= null){
            comment = (Comment) bundle.getSerializable("comment");
            comment.setComment(edComment.getText().toString().trim());
            smileRating.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
                @Override
                public void onRatingSelected(int level, boolean reselected) {
                    comment.setEvaluate((double) level);
                    Toast.makeText(Commenttaixe.this, level+"", Toast.LENGTH_SHORT).show();
                }
            });

            btnConfirn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myRef.child(comment.getIdComment()).setValue(comment);
                    Toast.makeText(Commenttaixe.this, edComment.getText().toString().trim(), Toast.LENGTH_SHORT).show();

                    AlertDialog.Builder builder = new AlertDialog.Builder(Commenttaixe.this);
                    builder.setTitle("Cảm ơn bạn đã đánh giá lái xe !");
                    builder.setIcon(R.drawable.ic_directions_bike_black_24dp);
                    builder.setCancelable(true);

                    builder.setPositiveButton("Xác nhận", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(Commenttaixe.this, MainActivity.class));
                            finish();
                        }
                    });
                    builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            });
        }
//        rbDanhgia.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
//            @Override
//            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
//                Toast.makeText(Commenttaixe.this, v+"", Toast.LENGTH_SHORT).show();
//            }
//        });


    }

    private void anhXa() {
        toolbarComment = findViewById(R.id.toolbarcomment);
        edComment = findViewById(R.id.comment);
    //        rbDanhgia = findViewById(R.id.danhgia);
        smileRating = (SmileRating) findViewById(R.id.danhgia);
        btnConfirn = findViewById(R.id.btnconfirn);
    }
}

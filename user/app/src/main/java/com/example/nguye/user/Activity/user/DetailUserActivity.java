package com.example.nguye.user.Activity.user;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.nguye.user.Activity.MainActivity;
import com.example.nguye.user.Model.User;
import com.example.nguye.user.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DetailUserActivity extends AppCompatActivity {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Users");
    TextView UName,Birthday,Address,Phone,Role,Email;
    Toolbar toolbar;
    ImageView imgEdit,avar_user;
    String id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_user);
        anhXa();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Chi tiết người dùng");
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DetailUserActivity.this,MainActivity.class));
                finish();
            }
        });

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("data");
        if (bundle != null) {
            id = bundle.getString("id");
            myRef.child(id).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User user =  dataSnapshot.getValue(User.class);
                    UName.setText(user.getUsername());
                    Birthday.setText(user.getBirthday());
                    Role.setText(user.getRole());
                    Email.setText(user.getEmail());
                    Phone.setText(user.getPhone());
                    Address.setText(user.getAddress());
                    Glide.with(DetailUserActivity.this).load(user.getAvatar()).into(avar_user);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailUserActivity.this, EditUserActivity.class);
                Bundle bundle = new Bundle();
//                bundle.putSerializable("user", (Serializable) userArrayList.get(position));
                bundle.putString("id", id);
                intent.putExtra("data", bundle);
                startActivity(intent);
            }
        });
    }

    private void anhXa() {
        UName = findViewById(R.id.usernamedetail);
        Birthday = findViewById(R.id.birthday);
        Role = findViewById(R.id.role);
        Address = findViewById(R.id.address);
        Phone = findViewById(R.id.phone);
        Email = findViewById(R.id.email);
        toolbar =  findViewById(R.id.toolbarqlUser);
        imgEdit = findViewById(R.id.imgedit);
        avar_user = findViewById(R.id.profile_image_user);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}

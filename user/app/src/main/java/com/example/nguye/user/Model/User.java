package com.example.nguye.user.Model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class User {
    private String address;
    private  String idUser;
    private String username;
    private String email;
    private String password;
    private String avatar;
    private String role;
    private String birthday;
    private String phone;
    private double lat;
    private double lng;
    private String vip;
    public User() {
    }

    public User(String idUser , String username, String email, String password) {
        this.idUser = idUser;
        this.username = username;
        this.email = email;
        this.password = password;
        this.avatar = "";
        this.role = "User";
        this.birthday = "";
        this.phone = "00000";
        this.lat = 0;
        this.lng = 0;
        this.vip = "vip1";
    }


    public User(String address, String idUser, String username, String email, String password, String avatar, String role, String birthday, String phone, double lat, double lng, String vip) {
        this.address = address;
        this.idUser = idUser;
        this.username = username;
        this.email = email;
        this.password = password;
        this.avatar = avatar;
        this.role = role;
        this.birthday = birthday;
        this.phone = phone;
        this.lat = lat;
        this.lng = lng;
        this.vip = vip;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }
}

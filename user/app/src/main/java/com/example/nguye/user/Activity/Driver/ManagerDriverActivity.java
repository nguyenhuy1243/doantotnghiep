package com.example.nguye.user.Activity.Driver;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.nguye.user.Activity.Oder.DetailsOderActivity;
import com.example.nguye.user.Activity.Oder.oderActivity;
import com.example.nguye.user.Adapter.DriverAdapter;
import com.example.nguye.user.Adapter.ItemClickSupport;
import com.example.nguye.user.Contants.Contant;
import com.example.nguye.user.Model.Driver;
import com.example.nguye.user.Model.Oder;
import com.example.nguye.user.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;

public class ManagerDriverActivity extends AppCompatActivity {

    Toolbar toolbarqlDriver;
    RecyclerView recycleviewDriver;
    RecyclerView.Adapter driverAdapter;
    ArrayList<Driver> driverList;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Drivers");
    private Oder oder;
    Intent intent;
    private TimePicker timePicker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_driver);
        anhXa();
        intent = getIntent();
        final Bundle bundle = intent.getBundleExtra("data");
        if(bundle!= null){
            oder = (Oder) bundle.getSerializable("oder");
        }

        recycleviewDriver.setHasFixedSize(true);
        recycleviewDriver.setLayoutManager(new LinearLayoutManager(this));
        setSupportActionBar(toolbarqlDriver);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarqlDriver.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        driverList = new ArrayList<>();
        driverAdapter = new DriverAdapter(driverList,this) ;
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                driverList.clear();
                for (DataSnapshot driverSnapshot : dataSnapshot.getChildren()) {
                    Driver driver = driverSnapshot.getValue(Driver.class);
                    driverList.add(driver);
                }
                recycleviewDriver.setAdapter(driverAdapter);
                driverAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        recycleviewDriver.setAdapter(driverAdapter);
        ItemClickSupport.addTo(recycleviewDriver).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClick(RecyclerView parent, View view, final int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ManagerDriverActivity.this);
                builder.setTitle("Bạn có chắc chắn muốn đi cùng chúng tôi chưa?");
                builder.setIcon(R.drawable.ic_distance);
                builder.setCancelable(true);
                View mView1 = getLayoutInflater().inflate(R.layout.alert_order,null);
                final EditText edPhonealer = mView1.findViewById(R.id.edphonealert);
                TextView txtpriceAlert  = mView1.findViewById(R.id.txtpricealert);
                txtpriceAlert.setText(Math.floor((oder.getDistance()* Contant.price))+"  vnd");
                oder.setPrice(Math.floor((oder.getDistance()* Contant.price)));
                builder.setPositiveButton("Xác nhận", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(edPhonealer.getText().toString().isEmpty()){
                            Toast.makeText(ManagerDriverActivity.this, "Số điện thoại bị bỏ trống", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            oder.setPhone(edPhonealer.getText().toString().trim());
                            Intent intentOder = new Intent(ManagerDriverActivity.this, oderActivity.class);
                            bundle.putSerializable("oder", (Serializable) oder);
                            intentOder.putExtra("data",bundle);
                            startActivity(intentOder);
                        }
                    }
                });
                builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.setView(mView1);
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

    }


    private void anhXa() {
        toolbarqlDriver = findViewById(R.id.toolbarqlDriver);
        recycleviewDriver = findViewById(R.id.recycleviewdriver);


    }
}

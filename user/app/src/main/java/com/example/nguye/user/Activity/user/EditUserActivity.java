package com.example.nguye.user.Activity.user;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.nguye.user.Activity.MainActivity;
import com.example.nguye.user.Model.User;
import com.example.nguye.user.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.Calendar;
import java.util.UUID;

public class EditUserActivity extends AppCompatActivity {
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Users");
    String id;
    Button btnChangeavar, btnLuued;
    ImageView avared;
    int flatCheckfile = 0;
    private Uri mAvarUri;
    private static final int PICK_IMAGE_REQUEST = 1;
    private StorageReference mStorageRef;
    EditText edUsername, edMail, edPhone, edBirthday, edAddress;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    User user;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);
        anhXa();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Chỉnh sửa người dùng");
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EditUserActivity.this,DetailUserActivity.class));
                finish();
            }
        });
        mStorageRef = FirebaseStorage.getInstance().getReference("Users");
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("data");
        if (bundle != null) {
            id = bundle.getString("id");
            myRef.child(id).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    user = dataSnapshot.getValue(User.class);
                    edUsername.setText(user.getUsername());
                    edMail.setText(user.getEmail());
                    edPhone.setText(user.getPhone());
                    edBirthday.setText(user.getBirthday());
                    edAddress.setText(user.getAddress());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
        edBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(EditUserActivity.this,
                        android.R.style.Theme_Holo_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = dayOfMonth + "/" + month + "/" + year;
                edBirthday.setText(date);
            }
        };
        btnChangeavar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChoose();
            }
        });
        btnLuued.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flatCheckfile == 1) {
                    uploadFile();
                }

            }
        });

    }

    private void uploadFile() {
        if (mAvarUri != null) {
            final StorageReference ref = mStorageRef.child("images/" + UUID.randomUUID().toString());
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();
            Task<Uri> urlTask = ref.putFile(mAvarUri).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return ref.getDownloadUrl();
                }
            })
                    .addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                Uri taskResult = task.getResult();
                                User updateUser = new User();
                                updateUser.setAddress(edAddress.getText().toString());
                                updateUser.setAvatar(taskResult.toString());
                                updateUser.setBirthday(edBirthday.getText().toString());
                                updateUser.setEmail(edMail.getText().toString());
                                updateUser.setIdUser(user.getIdUser());
                                updateUser.setLat(user.getLat());
                                updateUser.setLng(user.getLng());
                                updateUser.setPassword(user.getPassword());
                                updateUser.setPhone(edPhone.getText().toString());
                                updateUser.setRole(user.getRole());
                                updateUser.setUsername(edUsername.getText().toString());
                                myRef.child(id).setValue(updateUser);
                                progressDialog.dismiss();
                            }
                        }
                    });

        }
    }


    private void openFileChoose() {
        flatCheckfile = 1;
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            mAvarUri = data.getData();
            Log.d("abcc", String.valueOf(mAvarUri));
            Glide.with(this).load(mAvarUri).into(avared);

        }
    }

    private void anhXa() {
        btnChangeavar = findViewById(R.id.btnchangavar);
        avared = findViewById(R.id.avaredUser);
        btnLuued = findViewById(R.id.btnluued);
        edUsername = findViewById(R.id.edusernameed);
        edMail = findViewById(R.id.edemailed);
        edPhone = findViewById(R.id.edphoneed);
        edBirthday = findViewById(R.id.edbirthdated);
        edAddress = findViewById(R.id.edaddressed);
        toolbar = findViewById(R.id.toolbaredUser);
    }
}


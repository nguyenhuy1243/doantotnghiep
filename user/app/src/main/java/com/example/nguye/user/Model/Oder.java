package com.example.nguye.user.Model;

import java.io.Serializable;
import java.util.Date;

public class Oder implements Serializable {
    public String arrivalTime;
    public String originAddress;
    public String destinationAddress;
    public String idOder;
    public String idDriver;
    public String idUser;
    public String extimateTime;
    public String factTime;
    public String considerDriver;
    public Double price;
    public Double distance; //khoảng cách
    public String phone;
    public String vip;
    public String statusOder; //0 la dang dat 1 la xong 2 la huy
    public String date;

    public Oder() {
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getOriginAddress() {
        return originAddress;
    }

    public void setOriginAddress(String originAddress) {
        this.originAddress = originAddress;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getIdOder() {
        return idOder;
    }

    public void setIdOder(String idOder) {
        this.idOder = idOder;
    }

    public String getIdDriver() {
        return idDriver;
    }

    public void setIdDriver(String idDriver) {
        this.idDriver = idDriver;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getExtimateTime() {
        return extimateTime;
    }

    public void setExtimateTime(String extimateTime) {
        this.extimateTime = extimateTime;
    }

    public String getFactTime() {
        return factTime;
    }

    public void setFactTime(String factTime) {
        this.factTime = factTime;
    }

    public String getConsiderDriver() {
        return considerDriver;
    }

    public void setConsiderDriver(String considerDriver) {
        this.considerDriver = considerDriver;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public String getStatusOder() {
        return statusOder;
    }

    public void setStatusOder(String statusOder) {
        this.statusOder = statusOder;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
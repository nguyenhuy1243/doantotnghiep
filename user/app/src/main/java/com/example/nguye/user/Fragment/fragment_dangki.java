package com.example.nguye.user.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nguye.user.Model.User;
import com.example.nguye.user.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.concurrent.Executor;

public class fragment_dangki extends Fragment {
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    EditText edtUserName,edtEmailDK,edtPassword;
    Button btnDangKi;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register,container,false);
        anhXa(view);
        mAuth = FirebaseAuth.getInstance();


        btnDangKi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName,email,password;
                userName = edtUserName.getText().toString().trim();
                email = edtEmailDK.getText().toString().trim();
                password = edtPassword.getText().toString().trim();
                if(userName.isEmpty()||email.isEmpty()||password.isEmpty()){
                    Toast.makeText(getActivity(), "Không được bỏ trống", Toast.LENGTH_SHORT).show();
                }else{
                    DangKi(userName,email,password);
                }
            }
        });
        return view;
    }

    private void DangKi(final String userName, final String email, final String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            String idUser = FirebaseAuth.getInstance().getCurrentUser().getUid();
                            User user = new User(idUser,userName,email,password);
                            assert idUser != null;
                            mDatabase.child(idUser).setValue(user);
                            Toast.makeText(getActivity(), "Thanh cong", Toast.LENGTH_SHORT).show();

                        }else {
                            Toast.makeText(getActivity(), "That bai", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void anhXa(View view) {
        edtUserName = view.findViewById(R.id.edtUsernameDK);
        edtEmailDK = view.findViewById(R.id.edtEmailRegister);
        edtPassword = view.findViewById(R.id.edtPasswordDK);
        btnDangKi = view.findViewById(R.id.btnDangKi);
    }
}

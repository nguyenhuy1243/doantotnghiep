package com.example.nguye.driver.Fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nguye.driver.Adapter.ItemClickSupport;
import com.example.nguye.driver.Adapter.OderAdapter;
import com.example.nguye.driver.Model.Oder;
import com.example.nguye.driver.R;
import com.example.nguye.driver.activity.Oder.DetailOder;
import com.example.nguye.driver.contant.Contant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;

public class frangment_doanhso_byday  extends Fragment {
    String date_find;
    EditText edtDay;
    RecyclerView recycleviewOder;
    RecyclerView.Adapter oderAdapter;
    ArrayList<Oder> oderArrayList;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Oders");
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    int flat = 0;
    double Distance = 0;
    double price = 0;
    TextView txtSoLuong,txtquangduong,txtmoney;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_doanhso_byday,container,false);
        anhXa(view);
        oderArrayList = new ArrayList<Oder>();
        recycleviewOder.setLayoutManager(new LinearLayoutManager(getContext()));
        oderAdapter = new OderAdapter(oderArrayList,getContext()) ;
        edtDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flat = 1;
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(getContext(),
                        android.R.style.Theme_Holo_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

                }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = dayOfMonth + "/" + month + "/" + year;
                date_find = year + "/" + month + "/" + dayOfMonth;
                edtDay.setText(date);
                myRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        oderArrayList.clear();
                        for (DataSnapshot driverSnapshot : dataSnapshot.getChildren()) {
                            Oder oder = driverSnapshot.getValue(Oder.class);
                            if( oder.date.substring(0,10).toString().equals(date_find.toString()) && oder.statusOder.equals(Contant.DONE) ){
                                oderArrayList.add(oder);
                                oderAdapter.notifyDataSetChanged();
                            }
                        }
                        Distance = 0;
                        price = 0;
                        for(int i = 0 ;i<oderArrayList.size();i++){
                            Distance = Distance + oderArrayList.get(i).getDistance();
                            price = price+ oderArrayList.get(i).getPrice();
                        }
                        txtquangduong.setText(Distance+" km");
                        txtmoney.setText(price+ "00 VND");
                        txtSoLuong.setText(oderArrayList.size()+"");

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


                recycleviewOder.setAdapter(oderAdapter);
                oderAdapter.notifyDataSetChanged();
                ItemClickSupport.addTo(recycleviewOder).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClick(RecyclerView parent, View view, int position, long id) {

                        Intent intentDetailOder = new Intent(getContext(), DetailOder.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("idOder", oderArrayList.get(position).getIdOder());
                        intentDetailOder.putExtra("data",bundle);
                        startActivity(intentDetailOder);
                        Toast.makeText(getContext(),oderArrayList.get(position).getIdOder(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };



       return view;
    }


    private void anhXa(View view) {
        edtDay = view.findViewById(R.id.edday);
        txtSoLuong = view.findViewById(R.id.txtsoluong);
        recycleviewOder = view.findViewById(R.id.recycleOder);
        txtquangduong = view.findViewById(R.id.txtquangduong);
        txtmoney = view.findViewById(R.id.txtmoney);
    }
}


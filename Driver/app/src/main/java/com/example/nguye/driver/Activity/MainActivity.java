package com.example.nguye.driver.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nguye.driver.ModuleMap.DirectionFinder;
import com.example.nguye.driver.ModuleMap.DirectionFinderListener;
import com.example.nguye.driver.ModuleMap.Route;
import com.example.nguye.driver.activity.DoanhSo.DoanhSo;
import com.example.nguye.driver.activity.Oder.Commenttaixe;
import com.example.nguye.driver.activity.Oder.ManagerAllOder;
import com.example.nguye.driver.activity.Oder.ManagerOder;
import com.example.nguye.driver.R;
import com.example.nguye.driver.contant.Contant;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity    {
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;
    private Button btnbatdau;
    private final static int MY_PERMISSION_FINE_LOCATION = 101;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        anhXa();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Driver");
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(Gravity.START);
            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.manageroder:{
                        startActivity(new Intent(MainActivity.this, ManagerOder.class));
                        break;
                    }
                    case R.id.map_it:{
                        startActivity(new Intent(MainActivity.this,MapsActivity.class));
                        break;
                    }
                    case R.id.manageralloder:{
                        Log.e("ABC","2");
                        startActivity(new Intent(MainActivity.this, ManagerAllOder.class));
                        break;
                    }
                    case  R.id.taixe:{
                        Log.e("ABC","3");
                        Toast.makeText(MainActivity.this, "2", Toast.LENGTH_SHORT).show();
                        break;
                    }
                    case R.id.doanhso:{
                        startActivity(new Intent(MainActivity.this, DoanhSo.class));
                        break;
                    }

                    case R.id.dangxuat:{
                        Log.e("ABC","5");
                        Toast.makeText(getBaseContext(), "5", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                return false;
            }
        });
        btnbatdau.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,MapsActivity.class));
            }
        });
    }




    private void anhXa() {
        btnbatdau = findViewById(R.id.btnbatdau);
        drawerLayout = findViewById(R.id.dl);
        toolbar= findViewById(R.id.toolbar);
        navigationView= findViewById(R.id.nav_view);
    }
}

package com.example.nguye.driver.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.nguye.driver.Adapter.ViewpagerAdapterLogin;
import com.example.nguye.driver.R;

public class LoginActivity extends AppCompatActivity {
    ViewPager viewPager;
    TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        viewPager = findViewById(R.id.viewpagerLogin);
        tabLayout= findViewById(R.id.tabLayoutLogin);

        ViewpagerAdapterLogin adapter = new ViewpagerAdapterLogin(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}

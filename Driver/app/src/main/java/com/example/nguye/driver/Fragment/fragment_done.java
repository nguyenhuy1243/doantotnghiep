package com.example.nguye.driver.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.nguye.driver.Adapter.ItemClickSupport;
import com.example.nguye.driver.Adapter.OderAdapter;
import com.example.nguye.driver.Model.Oder;
import com.example.nguye.driver.R;
import com.example.nguye.driver.activity.Oder.DetailOder;
import com.example.nguye.driver.contant.Contant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class fragment_done extends Fragment {
    RecyclerView recycleviewOder;
    RecyclerView.Adapter oderAdapter;
    ArrayList<Oder> oderArrayList;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Oders");
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_done,container,false);
        anhXa(view);

        recycleviewOder.setHasFixedSize(true);
        recycleviewOder.setLayoutManager(new LinearLayoutManager(getContext()));
        oderArrayList = new ArrayList<>();
        oderAdapter = new OderAdapter(oderArrayList,getContext()) ;
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                oderArrayList.clear();
                for (DataSnapshot driverSnapshot : dataSnapshot.getChildren()) {
                    Oder oder = driverSnapshot.getValue(Oder.class);
                    if(oder.statusOder.equals(Contant.DONE)){
                        oderArrayList.add(oder);
                    }
                }
                recycleviewOder.setAdapter(oderAdapter);
                oderAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        recycleviewOder.setAdapter(oderAdapter);

        ItemClickSupport.addTo(recycleviewOder).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClick(RecyclerView parent, View view, int position, long id) {

                Intent intentDetailOder = new Intent(getContext(), DetailOder.class);
                Bundle bundle = new Bundle();
                bundle.putString("idOder", oderArrayList.get(position).getIdOder());
                intentDetailOder.putExtra("data",bundle);
                startActivity(intentDetailOder);
                Toast.makeText(getContext(),oderArrayList.get(position).getIdOder(), Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }


    private void anhXa(View view) {
        recycleviewOder = view.findViewById(R.id.recycleviewalloderdone);
    }
}
package com.example.nguye.driver.activity.DoanhSo;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import com.example.nguye.driver.Adapter.ViewpageAdapterDoanhso;
import com.example.nguye.driver.Adapter.ViewpagerAdapterOder;
import com.example.nguye.driver.R;

import java.util.Calendar;

public class DoanhSo extends AppCompatActivity {
    ViewPager viewPager;
    TabLayout tabLayout;
    Toolbar toolbardoanhso;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doanh_so);
        viewPager = findViewById(R.id.viewpagerdoanhso);
        tabLayout= findViewById(R.id.tabLayoutdoanhso);
        toolbardoanhso = findViewById(R.id.toolbardoanhso);
        //toolbar
        setSupportActionBar(toolbardoanhso);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Doanh số");
        toolbardoanhso.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        //tablayout
        ViewpageAdapterDoanhso adapter = new ViewpageAdapterDoanhso(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}


package com.example.nguye.driver.activity.Oder;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nguye.driver.Model.Comment;
import com.example.nguye.driver.Model.User;
import com.example.nguye.driver.R;
import com.example.nguye.driver.activity.MainActivity;
import com.example.nguye.driver.contant.Contant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hsalf.smilerating.SmileRating;

public class Commenttaixe extends AppCompatActivity {
    Toolbar toolbarComment;
    TextView edComment;
    Intent intent;
    private Comment comment;
    Button btnConfirn;
    RatingBar rbDanhgia;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Comments");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commenttaixe);
        anhXa();
        setSupportActionBar(toolbarComment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Đánh giá");
        toolbarComment.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbarComment.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Commenttaixe.this, MainActivity.class));
                finish();
            }
        });
        intent = getIntent();
        Bundle bundle = intent.getBundleExtra("data");
        if (bundle != null) {
            final String idOder = bundle.getString("idOder");
            myRef.child(idOder).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Comment commentTX =  dataSnapshot.getValue(Comment.class);
                    edComment.setText(commentTX.getComment());
                    rbDanhgia.setRating(commentTX.getEvaluate().intValue());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }
    }

        private void anhXa () {
            toolbarComment = findViewById(R.id.toolbarcomment);
            edComment = findViewById(R.id.comment);
            rbDanhgia = findViewById(R.id.danhgia);
        }
}


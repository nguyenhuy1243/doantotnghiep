package com.example.nguye.driver.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.nguye.driver.Fragment.fragment_cencle;
import com.example.nguye.driver.Fragment.fragment_doanhso_bymoth;
import com.example.nguye.driver.Fragment.fragment_done;
import com.example.nguye.driver.Fragment.fragment_pendding;
import com.example.nguye.driver.Fragment.frangment_doanhso_byday;

import java.util.ArrayList;
import java.util.List;

public class ViewpageAdapterDoanhso extends FragmentPagerAdapter {
    List<Fragment> listFragment = new ArrayList<Fragment>();
    List<String> tittleFragment = new ArrayList<String>();
    public ViewpageAdapterDoanhso(FragmentManager fm) {
        super(fm);
        listFragment.add(new frangment_doanhso_byday());
        listFragment.add(new fragment_doanhso_bymoth());

        tittleFragment.add("Doanh số theo ngày");
        tittleFragment.add("Doanh số theo tháng");
    }

    @Override
    public Fragment getItem(int position) {
        return listFragment.get(position);
    }

    @Override
    public int getCount() {
        return listFragment.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tittleFragment.get(position);
    }
}
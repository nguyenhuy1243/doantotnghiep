package com.example.nguye.driver.activity.Oder;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.nguye.driver.Adapter.ViewpagerAdapterLogin;
import com.example.nguye.driver.Adapter.ViewpagerAdapterOder;
import com.example.nguye.driver.R;

public class ManagerAllOder extends AppCompatActivity {
    ViewPager viewPager;
    TabLayout tabLayout;
    Toolbar toolbarAllOder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_all_oder);
        viewPager = findViewById(R.id.viewpageroder);
        tabLayout= findViewById(R.id.tabLayoutoder);
        toolbarAllOder = findViewById(R.id.toolbarallOder);
        //toolbar
        setSupportActionBar(toolbarAllOder);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Đơn hàng");
        toolbarAllOder.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        //tablayout
        ViewpagerAdapterOder adapter = new ViewpagerAdapterOder(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}

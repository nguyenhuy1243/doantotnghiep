package com.example.nguye.driver.activity.Oder;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.example.nguye.driver.Adapter.ItemClickSupport;
import com.example.nguye.driver.Adapter.OderAdapter;
import com.example.nguye.driver.Model.Oder;
import com.example.nguye.driver.R;
import com.example.nguye.driver.contant.Contant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;

public class ManagerOder extends AppCompatActivity {
    Toolbar toolbaroder;
    RecyclerView recycleviewOder;
    RecyclerView.Adapter oderAdapter;
    ArrayList<Oder> oderArrayList;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Oders");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_oder);
        anhXa();
        setSupportActionBar(toolbaroder);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Đơn hàng");
        toolbaroder.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        recycleviewOder.setHasFixedSize(true);
        recycleviewOder.setLayoutManager(new LinearLayoutManager(this));
        oderArrayList = new ArrayList<>();
        oderAdapter = new OderAdapter(oderArrayList,this) ;
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                oderArrayList.clear();
                for (DataSnapshot driverSnapshot : dataSnapshot.getChildren()) {
                    Oder oder = driverSnapshot.getValue(Oder.class);
                    if(oder.statusOder.equals(Contant.PENDING)){
                        oderArrayList.add(oder);
                    }
                }
                recycleviewOder.setAdapter(oderAdapter);
                oderAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        recycleviewOder.setAdapter(oderAdapter);
        ItemClickSupport.addTo(recycleviewOder).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClick(RecyclerView parent, View view, int position, long id) {

                Intent intentDetailOder = new Intent(ManagerOder.this, DetailOder.class);
                Bundle bundle = new Bundle();
                bundle.putString("idOder", oderArrayList.get(position).getIdOder());
                intentDetailOder.putExtra("data",bundle);
                startActivity(intentDetailOder);
                Toast.makeText(ManagerOder.this,oderArrayList.get(position).getIdOder(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void anhXa() {
        toolbaroder = findViewById(R.id.toolbarqlOder);
        recycleviewOder = findViewById(R.id.recycleviewqloder);
    }
}

package com.example.nguye.driver.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;


import com.example.nguye.driver.Model.Oder;
import com.example.nguye.driver.R;
import com.example.nguye.driver.contant.Contant;

import java.util.List;


public class OderAdapter  extends RecyclerView.Adapter<OderAdapter.ViewHolder>{
    List<Oder> oders;
    Context context;

    public OderAdapter(List<Oder> oders, Context context) {
        this.oders = oders;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_oder,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Oder oder = oders.get(i);
        viewHolder.txtdestinationAddress.setText(oder.destinationAddress);
        viewHolder.txtoriginAddress.setText(oder.originAddress);
        if(oder.statusOder.equals(Contant.PENDING)){
            viewHolder.txtstatus.setText(Contant.PENDING_txt);
        }else if(oder.statusOder.equals(Contant.CONFIRN)){
            viewHolder.txtstatus.setText(Contant.CONFIRN_txt);
            viewHolder.imgNew.setVisibility(View.INVISIBLE);
        }else if(oder.statusOder.equals(Contant.DONE)){
            viewHolder.txtstatus.setText(Contant.DONE_txt);
            viewHolder.imgNew.setVisibility(View.INVISIBLE);
        }else if(oder.statusOder.equals(Contant.CENCLE)){
            viewHolder.txtstatus.setText(Contant.CENCLE_txt);
            viewHolder.imgNew.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return oders.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtoriginAddress,txtdestinationAddress,txtstatus;
        ImageView imgNew;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtoriginAddress = itemView.findViewById(R.id.originaddress_it_oder);
            txtdestinationAddress = itemView.findViewById(R.id.destinationaddress_it_oder);
            txtstatus = itemView.findViewById(R.id.status_it_oder);
            imgNew = itemView.findViewById(R.id.new_it);
        }
    }
}

package com.example.nguye.driver.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nguye.driver.activity.MainActivity;
import com.example.nguye.driver.R;
import com.example.nguye.driver.contant.Contant;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class fragment_dangnhap extends Fragment {
    EditText edtEmailDN,edtPassword;
    Button btnDangNhap;
    private FirebaseAuth mAuth;
    private String id = "";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_login,container,false);
        btnDangNhap = view.findViewById(R.id.btnLogin);
        edtEmailDN = view.findViewById(R.id.edtEmaillogin);
        edtPassword = view.findViewById(R.id.edtPasswordlogin);
        mAuth = FirebaseAuth.getInstance();
        btnDangNhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = edtEmailDN.getText().toString().trim();
                String password = edtPassword.getText().toString().trim();
                if(email.isEmpty()||password.isEmpty()){
                    Toast.makeText(getActivity(), "không được bỏ trống", Toast.LENGTH_SHORT).show();
                }else{
                    mAuth.signInWithEmailAndPassword(email,password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if(task.isSuccessful()){
                                        Contant.idAuth = FirebaseAuth.getInstance().getCurrentUser().getUid();
                                        Toast.makeText(getActivity(), "thanh công", Toast.LENGTH_SHORT).show();
                                        Home();
                                    }else{
                                        Toast.makeText(getActivity(), "Thất bại", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }

            }
        });

        return view;
    }

    private void Home() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
        getActivity().finish();
    }

}

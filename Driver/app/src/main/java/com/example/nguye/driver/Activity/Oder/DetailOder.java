package com.example.nguye.driver.activity.Oder;

import android.content.Intent;
import android.location.Address;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.nguye.driver.Adapter.OderAdapter;
import com.example.nguye.driver.Model.Oder;
import com.example.nguye.driver.Model.User;
import com.example.nguye.driver.R;
import com.example.nguye.driver.activity.MapsActivity2;
import com.example.nguye.driver.contant.Contant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class DetailOder extends AppCompatActivity {
    Toolbar toolbardetailOrder;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Oders");
    EditText edArrivalOder,edvipOder;
    TextView txtoriginAddress,txtdestinationAddress,txtEstimatetime,txtphone,txtPriceOder,txtStatusOder;
    Button btnConfirn,btnCencle,btnComment;
    Oder oder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_oder);
        anhXa();
        setSupportActionBar(toolbardetailOrder);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Chi tiết");
        toolbardetailOrder.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("data");
        if (bundle != null) {
            final String idOder = bundle.getString("idOder");
            Log.e("id",idOder);
            myRef.child(idOder).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                     oder =  dataSnapshot.getValue(Oder.class);
                    edArrivalOder.setText(oder.getArrivalTime());
                    edvipOder.setText(oder.getVip());
                    txtdestinationAddress.setText(oder.getDestinationAddress());
                    txtoriginAddress.setText(oder.getOriginAddress());
                    //txtEstimatetime.setText(oder.getExtimateTime());
                    txtphone.setText(oder.getPhone());
                    txtPriceOder.setText(oder.getPrice()+"00 VND");
                    if(oder.getStatusOder().equals(Contant.PENDING)){
                        txtStatusOder.setText(Contant.PENDING_txt);
                        btnComment.setVisibility(View.INVISIBLE);
                    }else if(oder.getStatusOder().equals(Contant.CONFIRN)){
                        txtStatusOder.setText(Contant.CONFIRN_txt);
                        btnComment.setVisibility(View.INVISIBLE);
                    }else if(oder.getStatusOder().equals(Contant.DONE)){
                        txtStatusOder.setText(Contant.DONE_txt);
                        btnCencle.setVisibility(View.INVISIBLE);
                        btnConfirn.setVisibility(View.INVISIBLE);
                        btnComment.setVisibility(View.VISIBLE);
                    }else if(oder.getStatusOder().equals(Contant.CENCLE)){
                        txtStatusOder.setText(Contant.CENCLE_txt);
                        btnCencle.setVisibility(View.INVISIBLE);
                        btnConfirn.setVisibility(View.INVISIBLE);
                    }
                    btnConfirn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            oder.setStatusOder(Contant.CONFIRN);
                            myRef.child(idOder).setValue(oder);
                            Intent intentmap = new Intent(DetailOder.this, MapsActivity2.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("idOder", idOder);
                            intentmap.putExtra("data",bundle);
                            startActivity(intentmap);
                        }
                    });
                    btnCencle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            oder.setStatusOder(Contant.CENCLE);
                            myRef.child(idOder).setValue(oder);
//                            Intent intentmap = new Intent(DetailOder.this, MapsActivity2.class);
//                            Bundle bundle = new Bundle();
//                            bundle.putString("idOder", idOder);
//                            intentmap.putExtra("data",bundle);
//                            startActivity(intentmap);
                        }
                    });
                    btnComment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intentComment = new Intent(DetailOder.this, Commenttaixe.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("idOder", idOder);
                            intentComment.putExtra("data",bundle);
                            startActivity(intentComment);
                        }
                    });

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

    }

    private void anhXa() {
        toolbardetailOrder = findViewById(R.id.toolbardetailoder);
        edArrivalOder = findViewById(R.id.edarrivaloder);
        edvipOder = findViewById(R.id.edvipoder);
        txtdestinationAddress = findViewById(R.id.destinationaddress);
        txtoriginAddress = findViewById(R.id.originaddress);
        txtEstimatetime = findViewById(R.id.txtestimatetime);
        txtphone = findViewById(R.id.txtphone);
        txtPriceOder = findViewById(R.id.txtpriceoder);
        txtStatusOder = findViewById(R.id.txtstatusoder);
        btnCencle = findViewById(R.id.btncelcleoder);
        btnConfirn = findViewById(R.id.btnconfirnoder);
        btnComment = findViewById(R.id.btncomment);
    }
}

package com.example.nguye.driver.Model;

public class Driver {
    public  String address;
    public  String avatar;
    public  String birthday;
    public  String email;
    public  String idDriver;
    public  double lat;
    public  double lng;
    public  String password;
    public  String phone;
    public  String username;
    public  int danhGia;
    public  String status;

    public Driver() {
    }

    public Driver(String idDriver, String email, String username, String password) {
        this.idDriver = idDriver;
        this.email = email;
        this.password = password;
        this.username = username;
    }

    public Driver(String address, String avatar, String birthday, String email, String idDriver, double lat, double lng, String password, String phone, String username, int danhGia, String status) {
        this.address = address;
        this.avatar = avatar;
        this.birthday = birthday;
        this.email = email;
        this.idDriver = idDriver;
        this.lat = lat;
        this.lng = lng;
        this.password = password;
        this.phone = phone;
        this.username = username;
        this.danhGia = danhGia;
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdDriver() {
        return idDriver;
    }

    public void setIdDriver(String idDriver) {
        this.idDriver = idDriver;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getDanhGia() {
        return danhGia;
    }

    public void setDanhGia(int danhGia) {
        this.danhGia = danhGia;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

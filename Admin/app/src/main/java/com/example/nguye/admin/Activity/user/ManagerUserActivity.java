package com.example.nguye.admin.Activity.user;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.nguye.admin.Activity.MainActivity;
import com.example.nguye.admin.Adapter.ItemClickSupport;
import com.example.nguye.admin.Adapter.UserAdapter;
import com.example.nguye.admin.Model.User;
import com.example.nguye.admin.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ManagerUserActivity extends AppCompatActivity {
    Toolbar toolbarqluser;
    RecyclerView recycleviewUser;
    RecyclerView.Adapter userAdapter;
    ArrayList<User> userArrayList;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Users");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_user);
        anhXa();

        recycleviewUser.setHasFixedSize(true);
        recycleviewUser.setLayoutManager(new LinearLayoutManager(this));

        setSupportActionBar(toolbarqluser);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarqluser.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        userArrayList = new ArrayList<>();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userArrayList.clear();
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    User user = userSnapshot.getValue(User.class);
                    userArrayList.add(user);
                }
                recycleviewUser.setAdapter(userAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        ItemClickSupport.addTo(recycleviewUser).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClick(RecyclerView parent, View view, int position, long id) {
                Intent intent = new Intent(ManagerUserActivity.this, DetailuserActivity.class);
                Bundle bundle = new Bundle();
//                bundle.putSerializable("user", (Serializable) userArrayList.get(position));
                bundle.putString("id", userArrayList.get(position).getIdUser());
                intent.putExtra("data", bundle);
                startActivity(intent);
            }
        });
        ItemClickSupport.addTo(recycleviewUser).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(RecyclerView parent, View view, final int position, long id) {

                AlertDialog.Builder builder = new AlertDialog.Builder(ManagerUserActivity.this);
                builder.setTitle("Xóa người dùng!");
                builder.setIcon(R.drawable.ic_warning_black_24dp);
                builder.setCancelable(true);
                builder.setPositiveButton("Xóa", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        myRef.child(userArrayList.get(position).getIdUser()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                dataSnapshot.getRef().removeValue();
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                });
                builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                         dialog.cancel();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                return true;
            }
        });

        userAdapter = new UserAdapter(userArrayList, this);
        recycleviewUser.setAdapter(userAdapter);

    }

    private void anhXa() {
        toolbarqluser = findViewById(R.id.toolbarqlUser);
        recycleviewUser = findViewById(R.id.recycleviewuser);

    }
}

package com.example.nguye.admin.Activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.nguye.admin.Model.User;
import com.example.nguye.admin.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DetailuserActivity extends AppCompatActivity {
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Users");
    TextView UName,Birthday,Address,Phone,Role,Email;
    Toolbar toolbar;
    ImageView imgEdit,avar_user;
    String id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_user);
        anhXa();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Admin");
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("data");
        if (bundle != null) {
             id = bundle.getString("id");
            myRef.child(id).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User user =  dataSnapshot.getValue(User.class);
                    UName.setText(user.getUsername());
                    Birthday.setText(user.getBirthday());
                    Role.setText(user.getRole());
                    Email.setText(user.getEmail());
                    Phone.setText(user.getPhone());
                    Address.setText(user.getAddress());
                    Glide.with(DetailuserActivity.this).load(user.getAvatar()).into(avar_user);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailuserActivity.this, EditUserActivity.class);
                Bundle bundle = new Bundle();
//                bundle.putSerializable("user", (Serializable) userArrayList.get(position));
                bundle.putString("id", id);
                intent.putExtra("data", bundle);
                startActivity(intent);
            }
        });
    }

    private void anhXa() {
        UName = findViewById(R.id.usernamedetail);
        Birthday = findViewById(R.id.birthday);
        Role = findViewById(R.id.role);
        Address = findViewById(R.id.address);
        Phone = findViewById(R.id.phone);
        Email = findViewById(R.id.email);
        toolbar =  findViewById(R.id.toolbarqlUser);
        imgEdit = findViewById(R.id.imgedit);
        avar_user = findViewById(R.id.profile_image_user);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

}


package com.example.nguye.admin.Model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class User {
    private String address;
    private String avatar;
    private String birthday;
    private String email;
    private  String idUser;
    private double lat;
    private double lng;
    private String password;
    private String phone;
    private String role;
    private String username;

    public User() {
    }

    public User(String address, String avatar, String birthday, String email, String idUser, double lat, double lng, String password, String phone, String role, String username) {
        this.address = address;
        this.avatar = avatar;
        this.birthday = birthday;
        this.email = email;
        this.idUser = idUser;
        this.lat = lat;
        this.lng = lng;
        this.password = password;
        this.phone = phone;
        this.role = role;
        this.username = username;
    }

    public User(String id , String username, String email, String password) {
        this.idUser = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.avatar = "";
        this.role = "User";
        this.birthday = "";
        this.phone = "00000";
        this.lat = 0;
        this.lng = 0;
        this.address = "";
    }


    @Exclude
    public Map<String, Object> toMap(User user) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("username", user.username);
        result.put("password", user.password);
        result.put("email", user.email);
        result.put("avatar",user.avatar );
        result.put("role", user.role);
        result.put("phone", user.phone);
        result.put("lat", user.lat);
        result.put("lng", user.lng);
        result.put("address", user.address);

        return result;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}

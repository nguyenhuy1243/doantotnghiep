package com.example.nguye.admin.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.nguye.admin.Activity.driver.ManagerDriverActivity;
import com.example.nguye.admin.Activity.user.ManagerUserActivity;
import com.example.nguye.admin.R;

public class MainActivity extends AppCompatActivity {
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        anhXa();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(Gravity.START);
            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nguoidung:{
                        startActivity(new Intent(MainActivity.this, ManagerUserActivity.class));
                        break;
                    }
                    case  R.id.taixe:{
                        startActivity(new Intent(MainActivity.this, ManagerDriverActivity.class));
                        break;
                    }
                    case R.id.doanhso:{
                        Toast.makeText(MainActivity.this, "3", Toast.LENGTH_SHORT).show();
                        break;
                    }
                    case R.id.chuyendi:{
                        Toast.makeText(MainActivity.this, "3", Toast.LENGTH_SHORT).show();
                        break;
                    }
                    case R.id.dangxuat:{
                        Toast.makeText(MainActivity.this, "5", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                return false;
            }
        });
    }
    private void anhXa() {

        drawerLayout = findViewById(R.id.dl);
        toolbar= findViewById(R.id.toolbar);
        navigationView= findViewById(R.id.nav_view);

    }
}

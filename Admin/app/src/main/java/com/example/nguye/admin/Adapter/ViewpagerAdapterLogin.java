package com.example.nguye.admin.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.nguye.admin.Fragment.fragment_dangki;
import com.example.nguye.admin.Fragment.fragment_dangnhap;

import java.util.ArrayList;
import java.util.List;

public class ViewpagerAdapterLogin extends FragmentPagerAdapter {
    List<Fragment> listFragment = new ArrayList<Fragment>();
    List<String> tittleFragment = new ArrayList<String>();
    public ViewpagerAdapterLogin(FragmentManager fm) {
        super(fm);
        listFragment.add(new fragment_dangnhap());
        listFragment.add(new fragment_dangki());
        tittleFragment.add("Đăng Nhập");
        tittleFragment.add("Đăng Kí");
    }

    @Override
    public Fragment getItem(int position) {
        return listFragment.get(position);
    }

    @Override
    public int getCount() {
        return listFragment.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tittleFragment.get(position);
    }
}

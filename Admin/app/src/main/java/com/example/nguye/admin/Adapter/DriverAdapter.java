package com.example.nguye.admin.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.nguye.admin.Model.Driver;
import com.example.nguye.admin.Model.User;
import com.example.nguye.admin.R;

import java.util.List;

public class DriverAdapter  extends RecyclerView.Adapter<DriverAdapter.ViewHolder>{
    List<Driver> driverList;
    Context context;

    public DriverAdapter(List<Driver> driverList, Context context) {
        this.driverList = driverList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_driver,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
         Driver driver = driverList.get(position);
        Glide.with(context).load(driver.getAvatar()).into(holder.imgAvar);
        holder.txtUsername.setText(driver.getUsername());
        holder.ratingBarDG.setRating(driver.danhGia);
    }

    @Override
    public int getItemCount() {
        return driverList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgAvar;
        TextView txtUsername;
        RatingBar ratingBarDG;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgAvar = itemView.findViewById(R.id.img_avar_dr_it);
            txtUsername = itemView.findViewById(R.id.username_dr_it);
            ratingBarDG = itemView.findViewById(R.id.danhgia_dr_it);
        }
    }
}

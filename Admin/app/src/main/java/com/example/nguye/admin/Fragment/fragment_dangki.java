package com.example.nguye.admin.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nguye.admin.Model.User;
import com.example.nguye.admin.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class fragment_dangki extends Fragment {
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    EditText edtUserName,edtEmailDK,edtPassword;
    Button btnDangKi;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register,container,false);
        anhXa(view);
        mAuth = FirebaseAuth.getInstance();


        btnDangKi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName,email,password;
                userName = edtUserName.getText().toString().trim();
                email = edtEmailDK.getText().toString().trim();
                password = edtPassword.getText().toString().trim();
                if(userName.isEmpty()||email.isEmpty()||password.isEmpty()){
                    Toast.makeText(getActivity(), "Không được bỏ trống", Toast.LENGTH_SHORT).show();
                }else{

                    DangKi(userName,email,password);
                }
            }
        });
        return view;
    }

    private void DangKi(final String userName, final String email, final String password) {
        Log.d("test",userName+email+password);
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            mDatabase= FirebaseDatabase.getInstance().getReference().child("Users");
                            String id = mDatabase.push().getKey();
                            User user = new User(id,userName,email,password);
                            Log.d("asas",user.getEmail());
                            assert id != null;
                            mDatabase.child(id).setValue(user);
                            Toast.makeText(getActivity(), "Thanh cong", Toast.LENGTH_SHORT).show();

                        }else {
                            Toast.makeText(getActivity(), "That bai", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void anhXa(View view) {
        edtUserName = view.findViewById(R.id.edtUsernameDK);
        edtEmailDK = view.findViewById(R.id.edtEmailRegister);
        edtPassword = view.findViewById(R.id.edtPasswordDK);
        btnDangKi = view.findViewById(R.id.btnDangKi);
    }
}

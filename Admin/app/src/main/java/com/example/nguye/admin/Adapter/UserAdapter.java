package com.example.nguye.admin.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.nguye.admin.Model.User;
import com.example.nguye.admin.R;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder>{
    List<User> userList;
    Context context;

    public UserAdapter(List<User> userList, Context context) {
        this.userList = userList;
        this.context = context;
    }

    @NonNull
    @Override
    public UserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user,parent,false);
        return new ViewHolder(v);
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user = userList.get(position);
        Glide.with(context).load(user.getAvatar()). into(holder.imageAvar);
        holder.txtusername.setText(user.getUsername());
        holder.txtbirthday.setText(user.getBirthday());
        holder.txtaddress.setText(user.getAddress());

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtusername, txtbirthday, txtaddress;
        ImageView imageAvar;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageAvar = itemView.findViewById(R.id.profile_image_user_it);
            txtusername = itemView.findViewById(R.id.it_tenUser);
            txtbirthday = itemView.findViewById(R.id.it_birthday);
            txtaddress = itemView.findViewById(R.id.it_address);
        }

    }
}

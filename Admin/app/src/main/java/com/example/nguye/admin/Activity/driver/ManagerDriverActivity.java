package com.example.nguye.admin.Activity.driver;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.example.nguye.admin.Activity.user.DetailuserActivity;
import com.example.nguye.admin.Adapter.DriverAdapter;
import com.example.nguye.admin.Adapter.ItemClickSupport;
import com.example.nguye.admin.Model.Driver;
import com.example.nguye.admin.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ManagerDriverActivity extends AppCompatActivity {

    Toolbar toolbarqlDriver;
    RecyclerView recycleviewDriver;
    RecyclerView.Adapter driverAdapter;
    ArrayList<Driver> driverList;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Drivers");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_driver);
        anhXa();

        recycleviewDriver.setHasFixedSize(true);
        recycleviewDriver.setLayoutManager(new LinearLayoutManager(this));

        setSupportActionBar(toolbarqlDriver);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarqlDriver.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        driverList = new ArrayList<>();
        driverAdapter = new DriverAdapter(driverList,this) ;
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                driverList.clear();
                for (DataSnapshot driverSnapshot : dataSnapshot.getChildren()) {
                    Driver driver = driverSnapshot.getValue(Driver.class);
                    driverList.add(driver);
                }
                recycleviewDriver.setAdapter(driverAdapter);
                driverAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        recycleviewDriver.setAdapter(driverAdapter);


    }

    private void anhXa() {
        toolbarqlDriver = findViewById(R.id.toolbarqlDriver);
        recycleviewDriver = findViewById(R.id.recycleviewdriver);

    }
}
